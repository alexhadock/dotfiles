set encoding=utf-8

au VimEnter *  NERDTree

let NERDTreeCustomOpenArgs={'file':{'reuse':'currenttab'}}

let g:nerdtree_tabs_open_on_console_startup=1

let NERDTreeMapOpenInTab='\r'

map <Leader>n <plug>NERDTreeTabsToggle<CR>

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

if has('gui_running') " Глобальные настройки на GUI Vim
    set guioptions-=m " убираем меню
    " set guioptions-=e " убираем вкладки GUI делаем их как в консоли
    set guioptions-=T " убираем тулбар
    set guioptions-=r " убираем полосы прокрутки справа
    set guioptions-=L " убираем полосы прокрутки слева
    set background=light " Цвет фона темный или светлый
    set guioptions-=R
    set guioptions-=l
endif

set nu

set nocompatible

filetype off

set rtp+=~/.vim/bundle/vundle/

set clipboard=unnamed

call vundle#rc()
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
" тут будем добавлять наши расширения
Plug 'liuchengxu/vista.vim'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'tpope/vim-commentary'
Plugin 'dense-analysis/ale'
Plugin 'tpope/vim-fugitive'
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'kien/ctrlp.vim'
call vundle#end()         
filetype plugin indent on

colorscheme atom-dark
syntax on
